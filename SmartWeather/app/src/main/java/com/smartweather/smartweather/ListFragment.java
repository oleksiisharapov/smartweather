package com.smartweather.smartweather;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.smartweather.smartweather.weather.CityWeather;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ListFragment extends Fragment implements AdapterView.OnItemClickListener {


    private FragmentMediator fm;

    private List<CityWeather> cities;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fm = (FragmentMediator) getActivity();
        cities = new ArrayList<>();
        loadStoredData();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_fragment, null);
        ListView lv = (ListView) v.findViewById(R.id.itemList);
        lv.setOnItemClickListener(this);
        lv.setAdapter(new WeatherAdapter(getActivity(), R.layout.weather_item, cities.toArray(new CityWeather[]{})));


        return v;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Fragment f = fm.getFragmentByName("DetailFragment");
        Bundle b = f.getArguments();
        b.putString("cityId", Integer.toString(cities.get(position).getId()));
        b.putString("cityName", cities.get(position).getCityName());
        fm.changeCurrentFragment(f);
    }

    protected void loadStoredData() {
        BufferedReader br = null;
        String s;
        JSONObject jno;
        try {
            br = new BufferedReader(new InputStreamReader(getResources().openRawResource(R.raw.ukraine)));
            while ((s = br.readLine()) != null) {
                jno = new JSONObject(s);
                cities.add(new CityWeather(jno.getInt("_id"), jno.getString("name")));
            }
        } catch (Exception e) {
            Toast t = Toast.makeText(getActivity().getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT);
            t.show();
        }
    }
}
