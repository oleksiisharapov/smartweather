package com.smartweather.smartweather.view;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.Toast;

import com.smartweather.smartweather.R;


public class Gauge extends View {

    private int width;
    private int height;
    private Bitmap bmp, bmp2;
    private Paint p;
    private float angle = -30.0f;
    private int radius;
    private float rot  = 0.5f;
    float temperature;
    private String weatherIcon = "01d";

    public Gauge(final Context context, AttributeSet attrs) {
        super(context, attrs);

        Resources res = getResources();
        bmp = BitmapFactory.decodeResource(res, R.drawable.gauge);
        bmp2 = BitmapFactory.decodeResource(res, res.getIdentifier("i" + weatherIcon , "drawable", context.getPackageName()));
        p = new Paint();


        new Thread(new Runnable() {
            @Override
            public void run() {
                while (angle != (60 + temperature * 3)) {
                    try {
                        Thread.sleep(25);
                        if (angle == 211.f) {
                            rot = -rot;
                        } else if (angle == -31.f) {
                            rot = -rot;
                        }
                        angle += rot;
                        postInvalidate();
                    } catch (Exception e) {
                        Toast t = Toast.makeText(context.getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT);
                        t.show();
                    }
                }
            }
        }) {

        }.start();
    }




    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        width = bmp.getWidth();
        height = bmp.getHeight();
        radius = height / 3;
        canvas.drawBitmap(bmp, 0, 0, null);
        canvas.drawBitmap(bmp2, width / 2 - bmp2.getWidth()/2, 2 * height / 3, null);
        p.setColor(Color.RED);
        p.setStrokeWidth((float) 7);


        /* Из центра

        canvas.drawLine(width / 2, height / 2, (float) (width / 2  - radius * Math.cos((float) (angle * Math.PI / 180))),
                (float) (height / 2  - radius * Math.sin((float) (angle * Math.PI / 180))), p);
        */

        canvas.drawLine((float) (width / 2  - (radius/3) * Math.cos((float) (angle * Math.PI / 180))),
                (float) (height / 2  - (radius/3) * Math.sin((float) (angle * Math.PI / 180))),
                (float) (width / 2  - radius * Math.cos((float) (angle * Math.PI / 180))),
                (float) (height / 2  - radius * Math.sin((float) (angle * Math.PI / 180))), p);

    }

    public void setTemperature(float temperature){
        this.temperature = temperature;
    }

    public void setWeatherIcon(String weatherIcon){
        this.weatherIcon = weatherIcon;
        Resources res = getResources();
        bmp2 = BitmapFactory.decodeResource(res, res.getIdentifier("i" + weatherIcon , "drawable", getContext().getPackageName()));


    }



}
