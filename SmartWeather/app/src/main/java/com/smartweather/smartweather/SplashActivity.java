package com.smartweather.smartweather;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Thread t = new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(150);
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }catch (InterruptedException ie){
                    Toast t = Toast.makeText(getApplicationContext(), ie.getMessage(), Toast.LENGTH_SHORT);
                    t.show();
                }
            }
        });
        t.start();
    }
}
