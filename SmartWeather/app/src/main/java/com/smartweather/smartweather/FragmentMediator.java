package com.smartweather.smartweather;


import android.support.v4.app.Fragment;

public interface FragmentMediator {
    void changeCurrentFragment(Fragment f);
    Fragment getFragmentByName(String name);
}
