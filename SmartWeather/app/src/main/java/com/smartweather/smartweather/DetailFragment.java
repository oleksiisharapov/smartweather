package com.smartweather.smartweather;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.smartweather.smartweather.view.Gauge;
import com.smartweather.smartweather.weather.CityWeather;

import org.json.JSONObject;
import org.json.JSONArray;



import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class DetailFragment extends Fragment{


    String weatherIcon;
    CityWeather cw;
    Bundle b;
    String cityId;
    String cName;
    TextView cityName;
    TextView cityTemperature;
    TextView cityPressure;
    TextView cityWeatherConditions;
    TextView cityHumiduty;
    ProgressBar pb;
    Gauge gauge;

    UpdateWeather uw;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.detail_layout, null);


        //get cityID from bundle;
        try {
            cityId = getArguments().getString("cityId");
            cName = getArguments().getString("cityName");
        }catch(Exception e){
            Toast t = Toast.makeText(getContext().getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT);
            t.show();
        }

        gauge = (Gauge) v.findViewById(R.id.gauge);
        cityName = (TextView) v.findViewById(R.id.cityName);
        cityTemperature = (TextView) v.findViewById(R.id.cityTemp);
        cityPressure = (TextView) v.findViewById(R.id.cityPressure);
        cityWeatherConditions = (TextView) v.findViewById(R.id.cityWeatherCondition);
        cityHumiduty = (TextView) v.findViewById(R.id.cityHumiduty);
        pb = (ProgressBar) v.findViewById(R.id.progressBar);


        //Update weather info
        uw = new UpdateWeather();
        uw.execute();


        return v;
    }


    class UpdateWeather extends AsyncTask<Void, Void, Void>{


        String errorMessage;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pb.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            BufferedReader br;
            InputStream is;
            String urlAddres = getString(R.string.serverPath) + cityId + getString(R.string.appID);
            URL url;
            JSONObject jsonobj;
            StringBuilder sb = new StringBuilder();
            String s;

            try {

                url = new URL(urlAddres);
                is = url.openStream();
                br = new BufferedReader(new InputStreamReader(is));
                while((s = br.readLine()) != null){
                    sb.append(s);
                }
                br.close();
                is.close();


                jsonobj = new JSONObject(sb.toString());
                JSONArray weather = jsonobj.getJSONArray("weather");
                JSONObject jno = weather.getJSONObject(0);
                JSONObject main = jsonobj.getJSONObject("main");
                cw = new CityWeather(jsonobj.getInt("id"), jsonobj.getString("name"), jno.getString("description"), main.getDouble("temp"),
                        main.getInt("humidity"), main.getInt("pressure"), jno.getString("icon") );
                weatherIcon = jno.getString("icon");
            }catch (Exception e){
                errorMessage = new String(e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pb.setVisibility(View.GONE);
            cityName.setText(cName);
            if(errorMessage == null) {
                cityName.setText(cw.getCityName());
                cityTemperature.setText(Double.toString(cw.getTemp()));
                cityPressure.setText(Integer.toString(cw.getPressure()) + "kPa");
                cityWeatherConditions.setText(cw.getWeatherCondition());
                cityHumiduty.setText(Integer.toString(cw.getHumidity()) + "%" + "\n " + weatherIcon);
                gauge.setTemperature((float) Math.round(cw.getTemp()) );
                gauge.setWeatherIcon(cw.getIcoName());
            }else{
                Toast t = Toast.makeText(getActivity().getApplicationContext(), errorMessage, Toast.LENGTH_SHORT);
                t.show();
            }

        }
    }



}
