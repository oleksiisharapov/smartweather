package com.smartweather.smartweather.weather;


public class CityWeather {
    private int id;
    private String cityName;
    private String weatherCondition;
    private double temp;
    private int humidity;
    private int pressure;
    private String icoName = "01d";

    public CityWeather(){};

    public CityWeather(int id, String cityName){
        this.id = id;
        this.cityName = cityName;
    }

    public CityWeather(int id, String cityName, String weatherCondition, double temp, int humidity, int pressure, String icoName) {
        this.id = id;
        this.cityName = cityName;
        this.weatherCondition = weatherCondition;
        this.temp = temp;
        this.humidity = humidity;
        this.pressure = pressure;
        this.icoName = icoName;
    }

    public String getCityName(){

        return this.cityName;
    }

    public double getTemp(){
        return this.temp;
    }

    public int getId() {
        return id;
    }

    public String getWeatherCondition() {
        return weatherCondition;
    }

    public int getHumidity() {
        return humidity;
    }

    public int getPressure() {
        return pressure;
    }

    public String getIcoName() {
        return icoName;
    }

    public String getWeather(){
        StringBuilder sb = new StringBuilder();
        sb.append(cityName);
        sb.append("\n");
        sb.append(weatherCondition);
        sb.append("\n");
        sb.append(temp);
        sb.append("°C");
        sb.append("\n");
        sb.append(humidity);
        sb.append("%");
        sb.append("\n");
        sb.append(pressure);
        sb.append("kPa");
        return sb.toString();
    }
}
