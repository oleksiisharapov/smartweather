package com.smartweather.smartweather;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartweather.smartweather.weather.CityWeather;


public class WeatherAdapter extends ArrayAdapter<CityWeather>{

    private Context context;
    private int resourceId;
    private CityWeather[] cities;


    public WeatherAdapter(Context context, int resource, CityWeather[] objects) {
        super(context, resource, objects);
        this.context = context;
        this.resourceId = resource;
        this.cities = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater li = ((Activity)context).getLayoutInflater();
        View v = li.inflate(R.layout.weather_item, null, true);
        ImageView iv = (ImageView)  v.findViewById(R.id.conditionImage);
        TextView cityName = (TextView) v.findViewById(R.id.cityName);
        TextView cityTemperature = (TextView) v.findViewById(R.id.cityTemperature);

        cityName.setText(cities[position].getCityName());
        cityTemperature.setText(String.valueOf(cities[position].getTemp()));
        Resources res = getContext().getResources();
        iv.setImageResource(res.getIdentifier("i" + cities[position].getIcoName() , "drawable", context.getPackageName()));

        return v;
    }
}
