package com.smartweather.smartweather;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements FragmentMediator, ListView.OnItemClickListener {



    private Map<String, Fragment> fragments;
    private FragmentTransaction ft;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        fragments = new HashMap<>();

        ListFragment lf = new ListFragment();
        Bundle b = new Bundle();
        lf.setArguments(b);
        fragments.put("ListFragment", lf);

        b = new Bundle();
        DetailFragment df = new DetailFragment();
        df.setArguments(b);
        fragments.put("DetailFragment", df);


        ListView lv = (ListView) findViewById(R.id.menuDrawer);
        lv.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, fragments.keySet().toArray(new String[fragments.size()])));
        lv.setOnItemClickListener(this);



        ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.add(R.id.fragmentHandler, lf);
        ft.commit();

        DrawerLayout dr = (DrawerLayout) findViewById(R.id.activity_main);
        dr.openDrawer(GravityCompat.START);

    }




    @Override
    public void changeCurrentFragment(Fragment f) {
        ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.start, R.anim.end, R.anim.start, R.anim.end);
        ft.replace(R.id.fragmentHandler, f);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public Fragment getFragmentByName(String name) {

        return fragments.get(name);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast t = Toast.makeText(getApplicationContext(), fragments.keySet().toArray(new String[fragments.size()])[position], Toast.LENGTH_SHORT);
        t.show();
    }
}
